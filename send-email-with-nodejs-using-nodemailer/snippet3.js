transporter.sendMail(mailOptions, function (error, info) {
	if (error) {
		console.log('Technical problem. Can not send email now. Please try later.');
	} else {
		console.log('Message sent');
	}
});